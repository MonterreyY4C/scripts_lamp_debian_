#!/usr/bin/bash

DOMINIO=$1
SUBDOMINIO=$2

cp vhost.conf /etc/httpd/conf.d/"${SUBDOMINIO}".conf

sed -i "s/servidor/${HOSTNAME}/g" /etc/httpd/conf.d/"${SUBDOMINIO}".conf

sed -i "s/PROYECTOS/\/var\/www\/html\/proyectos\/${SUBDOMINIO}\/wordpress/g" /etc/httpd/conf.d/"${SUBDOMINIO}".conf

sed -i "s/DOMINIO/${DOMINIO}/g" /etc/httpd/conf.d/"${SUBDOMINIO}".conf

sed -i "3i\127.0.0.1 \t${DOMINIO}" /etc/hosts

systemctl reload httpd