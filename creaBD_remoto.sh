#!/usr/bin/bash

#------- Parametros recibidos
DBNAME=$1
DBUSER=$2
USERPASS=$3
REMOTE_USER=$4
REMOTE_IP=$5
PASSWORD=$6


#------- Variables
LOCAL_IP=$(ip route get 1.1.1.1 | awk '{print $7}')

#-------- Comandos para crear la BD
crea_bd () {
    QUERY1="mysql -u root -e \"CREATE DATABASE ${DBNAME}\" -p'${PASSWORD}'"
    QUERY2="mysql -u root -e \"CREATE USER '${DBUSER}'@'${LOCAL_IP}' IDENTIFIED BY '${USERPASS}'\" -p'${PASSWORD}'"
    QUERY3="mysql -u root -e \"GRANT ALL PRIVILEGES ON ${DBNAME}.* TO '${DBUSER}'@'${LOCAL_IP}'\" -p'${PASSWORD}'"
    QUERY4="mysql -u root -e \"FLUSH PRIVILEGES\" -p'${PASSWORD}'"
    #echo "${QUERY}"

    ssh "${REMOTE_USER}"@"${REMOTE_IP}" "${QUERY1}"
    ssh "${REMOTE_USER}"@"${REMOTE_IP}" "${QUERY2}"
    ssh "${REMOTE_USER}"@"${REMOTE_IP}" "${QUERY3}"
    ssh "${REMOTE_USER}"@"${REMOTE_IP}" "${QUERY4}"
}
#################### FIN FUNCIONES ######################

########### MAIN ###########
crea_bd
